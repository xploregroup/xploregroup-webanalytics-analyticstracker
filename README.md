# Analyticstracker

Analyticstracker is a client-side Javascript module for handling the [Event Driven Web Analytics Tracking](https://bitbucket.org/xploregroup/xploregroup-webanalytics-demo).

## Prerequisites

Analyticstracker is depending on [mediator-js](http://thejacklawson.com/Mediator.js/)

and requires one or more translators for passing the events

  * to Google Tag Manager:  [analytics-transGTM](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transGTM)
  * to Google Tag Manager Enhanced E-commerce: [analytics-transGTMEE](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transGTMEE)
  * to Adobe Dynamic Tag Manager: [analytics-transDTM](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transDTM)
  * to Chrome Devtool for QA: [analytics-transQA](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transQA)
  * to MongoDB Atlas: [analytics-transATLAS](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transATLAS)
  * to iFrame parent (via postMessage): [analytics-transIFrame](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analytics-transiframe)


## Debugging tool
GDDL Inspector is a Chrome WebDevelopment extension, allowing to view the GDDL events, and HTML attribution (if applicable).
You can download it from the Chrome Web Store: [GDDL Inspector](https://chrome.google.com/webstore/detail/gddl-inspector/glemmkkipabbechhnlmfiddhebdmandc)


## Getting Started

Download the script and load it directly in HTML with a script tag:

```
<script type="text/javascript" src="mediator.min.js"></script>
<script type="text/javascript" src="analyticstracker.min.js"></script>
<script type="text/javascript" src="analyticsTransQA.min.js"></script>
```

or require it in your preferred script loader (e.g. require.js)

```
require(['mediator-js', 'analyticstracker', 'analyticsTransQA'], function($) {
	if (module = $('script[src$="require.js"]').data('module')) {
		require([module]);
	}
});
```

and start tracking:

```
var atrack = analyticstracker();

$(document).ready(function(){
  atrack.trackImpression("page-impression");
});

$('[data-tracking-event$=-click]').click(atrack.trackInteraction);
```

## iFrame Tracking
The analyticstracker supports iFrame tracking through a window.PostMessage solution. The embedded iFrame will send a postMessage to the parent with the standard event context as defined in this document. The parent will receive the event:
* parent page must be initialized in the iFrame context, and iFrame URL must be initialized in the parent page to allow postMessage bi-directional validation
* the page-impression of the iFrame will be translated to an iframe-impression event
* the pageInfo of the iFrame is converted to an iframeInfo
* every event called in the iFrame will have the iframeInfo context added and the pageInfo of the parent page

example:
* iFrame loading in parent page
```
  <div class="col-sm-7">
    <iframe src="http://iframe.mytest2.local:3000?iframed=http%3A%2F%2Fwww.mytest.local%3A3000" width="400px" height="400px" frameborder="0"></iframe>
  </div>
```

* iFrame hosted page tracking code (requires the analyticsTransIFrame translator to be loaded)
```
  var atrack = analyticstracker();
  ...
  if(location.search.indexOf("iframed") > -1) {
    // get the analyticstracker iFrame translator
    var aIframeTrans = analyticsTransIFrame();

    /* register the parent URL:
     * it is possible to register a wildcard if the iFrame can be loaded
     * in multiple parent pages, but for security reasons it is advised to 
     * only add the exact calling domains
     */

    // Wildcard:
    // aIframeTrans.addParentUrl("*");

    // Direct (best):
    // the URL passed may only contain protocol://domain[:port])
    // aIframeTrans.addParentUrl("http://www.mytest.local:3000");

    // Passed on URL parameter
    aIframeTrans.addParentUrl(getParamsMap()["iframed"]);
  }
  ...
```
Note: if the pages called as iFrames can also be called directly as a seperate website, normal tracking is also possible in parallel with the iFrame tracking.

* parent page tracking
```
  var atrack = analyticstracker();
  ...
  if (document.location.pathname == "/iframed.html") {
    var allowedIframes = [];
    // the URL passed may only contain protocol://domain[:port])
    allowedIframes.push("http://iframe.mytest2.local:3000");
    atrack.trackIFrames(allowedIframes);
  }
```

* event from iFrame (in parent page tracking)
```
eventData: {
 "event": "iframe-impression",
 "iframeInfo": {
  "name": "iframed form",
  "url": "http://iframe.mytest2.local:3000/?iframed=http%3A%2F%2Fwww.mytest.local%3A3000",
  "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
  "platform": "MacIntel",
  "domain": "iframe.mytest2.local",
  "referrer": "http://www.mytest.local:3000/iframed.html",
  "title": "Analytics VT IFrame",
  "path": "/",
  "timestamp": "2019-04-09 12:03:11",
  "params": {
   "iframed": "http://www.mytest.local:3000"
  },
  "consent": "3"
 },
 "pageInfo": {
  "url": "http://www.mytest.local:3000/iframed.html",
  "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
  "platform": "MacIntel",
  "domain": "www.mytest.local",
  "referrer": "",
  "title": "Analytics demo Campaign",
  "path": "/iframed.html",
  "timestamp": "2019-04-09 12:03:11",
  "params": {},
  "consent": "3"
 }
}
```
Note: if the Iframe is hosted on the same domain, do not load the transQA module on the Iframed page, since it will produce ambiguous events logged in the GDDL inspector.

## Methods

**analyticstracker()**

Requires a Singleton instance of the tracking object
```
var atrack = analyticstracker();
```

**trackImpression(eventSelector [, options])**

Track all DOM elements which have the 'eventSelector' event data attribute. Optional options (see Event options) can be added to the handler to change the default behavior of the interaction event.
```
$(document).ready(function(){
  atrack.trackImpression("page-impression");
});
```

**trackInteraction(event [, options])**

Call in an event handler for an interaction event. Optional options (see Event options) can be added to the handler to change the default behavior of the interaction event.
```
$('[data-tracking-event$=-click]').click(atrack.trackInteraction);

$('[data-tracking-event=form-focus]').focus(atrack.trackInteraction);
$('[data-tracking-event=form-focus]').change(function(e) {
      atrack.trackInteraction(e, {"changeEvent" : "form-fieldcomplete", "collectFormData" : ["username", "email"]});
    });
```

**trackElement(elementSelection)**

Pass a selected NodeList HTML element directly to the tracker. Is useful when you need to make the selection for the DOM element specifically.
```
atrack.trackElement($('#myCarousel li[data-tracking-event="product-impression"].active').not('[data-tracking-done="true"]'));
```


**trackEvent(rawEventData)**

Pass a JSON style raw event directly to the tracking
```
var trackingEvent = {"event": "banner-click" , "info" : {"name" : "promoBanner.jpg", "position" : "top", "incentive": "summerPromo"}};
atrack.trackEvent(trackingEvent);
```

#### Event Options
| Option  | Description | Default | Example |
| :--- | :--- | :--- | :--- |
| checkVisibility  | check if the DOM element is currently in the browser view  | true  | {"checkVisibility" : false} |
| doCollect  | collect impression events of the same selector type, to reduce the number of event calls   | false  | {"doCollect" : true} |
| addData  | add user data to the event  | none | {"addData" : {"userId": "aqkj56ssk"}}  |
| changeEvent  | change the original event name before passing it down the chain  | none | {"changeEvent" : "form-fieldcomplete"}  |
| collectFormData  | specify if and which fields to collect from a form. The keyword "all" can be used to collect all form fields  | none | {"collectFormData" : ["username", "email"]}}
| newPage  | in a Single Page App, use this flag to indicate a "new page request". This will reset any event or impression history.  | false  | {"newPage" : true} |
| extendPageInfo  | collect page context directly from document and navigator. This data will automatic merged with passed page context data. User defined data will take precedence. | ['url', 'userAgent', 'platform', 'domain', 'referrer', 'title', 'path','params'] | {"extendPageInfo" : ['domain', 'url']} |

Multiple options can be specified within one call, if it makes sense.

#### Default HTML selectorSettings
The analyticstracking is based on a default data attribution syntax for selecting events and gattering the event data. These selectors can be modified if the implementation would require it.

```
atrack.setHTMLSelectors({"eventSelector" : "data-event", infoSelector" : "data-analytics"});

```
| Option  | Default |
| :--- | :--- |
| eventSelector | data-tracking-event |
| infoSelector | data-tracking-info |
| commerceSelector | data-tracking-commerce |
| pageSelector | page-impression |
| doneSelector | data-tracking-done |
| extendPageInfo | ['url', 'userAgent', 'platform', 'domain', 'referrer', 'title', 'path', 'params'] |
| formSubmitSelector | form-submit |

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Changelog
##### version 1.0.6
* fix in iFrame tracking, limit captured events to GDDL events

##### version 1.0.5
* Inview checking triggers when element taller than viewport is entering viewport

##### version 1.0.4
* document.location.hash is automatic added to pageInfo

##### version 1.0.3
* fix in UMD/AMD module load if window does not exist (Gatsby SSR)

##### version 1.0.2
* URL parameters are URIDecoded by default
* added iFrame tracking support

##### version 1.0.1
* added consentcookie extension
* added function to pre-check visibility
* added overrule function for visibility check (headless testing)
* fix in runQueue on internalTrackingHistory when event was fired before page-impression

##### version 1.0.0
* Intitial version

## Authors

* **Stefan Maris** - *Initial work* - [Xplore Group](http://www.xploregroup.be)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Based on POC implementation at Essent.nl
