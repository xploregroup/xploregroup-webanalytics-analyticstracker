/*!
* Analyticstracker.js Library
*/
(function (root, factory) {

  'use strict';

  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    // define(['mediator-js'], factory);
    define(["mediator-js"], function(Mediator) {
     return (root.analyticstracker = factory(Mediator, (typeof window !== "undefined" ? window : this)));
   });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like enviroments that support module.exports,
    // like Node.
    module.exports = factory(require('mediator-js').Mediator, (typeof window !== "undefined" ? window : this));
  } else {
    // Browser globals
    root.analyticstracker = factory(root.Mediator, window);
  }

}(this, function (Mediator, window) {
// UMD Definition above, do not remove this line
  'use strict';
  var _instance;

  var document = window.document;
  var navigator = window.navigator;

  var HTMLSelector = function HTMLSelector(selectorSettings) {
    if (!(this instanceof HTMLSelector)) {
      return new HTMLSelector();
    }

    this.eventSelector = (selectorSettings.hasOwnProperty("eventSelector")) ? selectorSettings.eventSelector : "data-tracking-event";
    this.infoSelector = (selectorSettings.hasOwnProperty("infoSelector")) ? selectorSettings.infoSelector : "data-tracking-info";
    this.commerceSelector = (selectorSettings.hasOwnProperty("commerceSelector")) ? selectorSettings.commerceSelector : "data-tracking-commerce";
    this.pageSelector = (selectorSettings.hasOwnProperty("pageSelector")) ? selectorSettings.pageSelector : "page-impression";
    this.doneSelector = (selectorSettings.hasOwnProperty("doneSelector")) ? selectorSettings.doneSelector : "data-tracking-done";
    this.extendPageInfo = (selectorSettings.hasOwnProperty("extendPageInfo")) ? selectorSettings.extendPageInfo : ['url', 'userAgent', 'platform', 'domain', 'referrer', 'title', 'path', 'hash', 'timestamp', 'params', 'consent'];
    this.formSubmitSelector = (selectorSettings.hasOwnProperty("formSubmitSelector")) ? selectorSettings.formSubmitSelector : "form-submit";
    this.iframeSelector = (selectorSettings.hasOwnProperty("iframeSelector")) ? selectorSettings.iframeSelector : "iframe-impression";
    this.consentCookie = "cookieconsent";
    this.disableVisibilityCheck = false;
  }

  HTMLSelector.prototype._splitUri = function(uri) {
    var splitRegExp = new RegExp(
        '^' +
            '(?:' +
            '([^:/?#.]+)' +                         // scheme - ignore special characters
                                                    // used by other URL parts such as :,
                                                    // ?, /, #, and .
            ':)?' +
            '(?://' +
            '(?:([^/?#]*)@)?' +                     // userInfo
            '([\\w\\d\\-\\u0100-\\uffff.%]*)' +     // domain - restrict to letters,
                                                    // digits, dashes, dots, percent
                                                    // escapes, and unicode characters.
            '(?::([0-9]+))?' +                      // port
            ')?' +
            '([^?#]+)?' +                           // path
            '(?:\\?([^#]*))?' +                     // query
            '(?:#(.*))?' +                          // fragment
            '$');

      var split;
      split = uri.match(splitRegExp);

      return {
            'protocol':split[1],
            'user_info':split[2],
            'domain':split[3],
            'port':split[4],
            'pathname':split[5],
            'search': split[6],
            'hash':split[7]
      }
  }

  HTMLSelector.prototype.setPageInfoField = function (info, field) {
    function pad(n) {
      return n<10 ? '0'+n : n;
    }

    // if info.url is predefined, use this one
    var overWriteUrl = (info.url) ? this._splitUri(info.url) : false;

    switch (field) {
      case "url" :
          info.url = (overWriteUrl !== false) ? info.url : document.URL;
          break;
      case "domain" : info.domain = (overWriteUrl !== false) ? overWriteUrl.domain : document.domain;
          break;
      case "referrer" : info.referrer = document.referrer;
          break;
      case "title" : info.title = document.title;
          break;
      case "userAgent" : info.userAgent =  navigator.userAgent;
        break;
      case "platform" : info.platform = navigator.platform;
          break;
      case "path" : info.path = (overWriteUrl !== false) ? overWriteUrl.pathname : document.location.pathname;
          break;
      case "hash" :
            if (document.location.hash) {
              info.hash = document.location.hash;
            } else {
              info.hash = (overWriteUrl !== false) ?  overWriteUrl.hash : "";
            }
          break;
      case "timestamp" : var d = new Date();
            info.timestamp = d.getFullYear() + "-" + pad((d.getMonth() + 1)) + "-" + pad(d.getDate()) + " " + pad(d.getHours()) + ":" + pad(d.getMinutes()) + ":" + pad(d.getSeconds());
          break;
      case "params" :
          info.params = {};
          if (document.location.search) {
            document.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
              info.params[key] = decodeURIComponent(value);
              }
            );
          } else {
            if (overWriteUrl !== false) {
              if (overWriteUrl.search) {
                overWriteUrl.search.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function(match, key, value) {
                     info.params[key] = decodeURIComponent(value);
                });
              }
            }
          }
          break;
      case "consent" :
          info.consent = this._getConsentLevelFromCookie();
          break;
    }

    return info;
  }

  HTMLSelector.prototype.isVisible = function (elem) {
    // overrule visibilityCheck for headless testing
    if (this.disableVisibilityCheck) {return true;}
    
    var style = getComputedStyle(elem);
    if (style.display === 'none') return false;
    if (style.visibility !== 'visible') return false;
    if (style.opacity < 0.1) return false;
    if (elem.offsetWidth + elem.offsetHeight + elem.getBoundingClientRect().height +
        elem.getBoundingClientRect().width === 0) {
        return false;
    }
    // check if elem heigher then client window
    if (elem.offsetHeight >= (document.documentElement.clientHeight || window.clientHeight)) {
      // trigger when part of element is in the viewport
      if ((elem.getBoundingClientRect().top > 0) && (elem.getBoundingClientRect().top > (document.documentElement.clientHeight || window.clientHeight))) return false;
      if ((elem.getBoundingClientRect().top < 0) && ((elem.getBoundingClientRect().top + elem.getBoundingClientRect().height) < 0)) return false;
      return true;
    }

    // center of elem check
    var elemCenter   = {
        x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
        y: elem.getBoundingClientRect().top + elem.offsetHeight / 2
    };
    if (elemCenter.x < 0) return false;
    if (elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)) return false;
    if (elemCenter.y < 0) return false;
    if (elemCenter.y > (document.documentElement.clientHeight || window.clientHeight)) return false;
    var pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y);
    do {
        if (pointContainer === elem) return true;
    } while (pointContainer = pointContainer.parentNode);
    return false;
  }

  HTMLSelector.prototype._selectElements = function(selector) {
    return document.querySelectorAll(selector);
  }

  HTMLSelector.prototype._getItemType = function (item) {
    var event = item.getAttribute(this.eventSelector);
    if (event == this.pageSelector) return "page-impression";
    if (event.match(/impression/g)) return "impression";
    return "interaction";
  }

  HTMLSelector.prototype._getFormFieldData = function (formItem) {
    switch (formItem.tagName) {
      case 'INPUT' :
        if (formItem.hasAttribute("type") && ((formItem.getAttribute("type") == "radio") || (formItem.getAttribute("type") == "checkbox"))) {
          return (formItem.checked) ? formItem.value : "";
        } else {
          return formItem.value;
        }
        break;
      case 'TEXTAREA' :
          return formItem.value;
        break;
      case 'SELECT' :
          return formItem.value;
        break;
    }
  }

  HTMLSelector.prototype._inArray = function (item, collectList) {
    for (var i = 0, ilen = collectList.length; i < ilen; i++) {
      if (item == collectList[i]) return true;
    }
    return false;
  }

  HTMLSelector.prototype._getFormData = function (item, collectList) {
    var formData = {};
    if (item.tagName == "FORM") {
      var formElements = item.elements;
      for (var j = 0, jlen = formElements.length; j < jlen; j++) {
        var formItem = formElements[j];
        if (formItem.hasAttribute(this.infoSelector)) {
          var formInfo = JSON.parse(formItem.getAttribute(this.infoSelector));
          var fieldName = formInfo.field;
          if (this._inArray("all", collectList) || this._inArray(fieldName, collectList)) {
            formData[fieldName] = formData[fieldName] || "";
            formData[fieldName] += ((this._getFormFieldData(formItem) == "") ? "" : this._getFormFieldData(formItem) + ",");
          }
        }
      }
      for (var anItem in formData) {
        if (formData.hasOwnProperty(anItem)) {
          formData[anItem] = formData[anItem].slice(0, -1);
        }
      }
    }
    else {
      var formInfo = JSON.parse(item.getAttribute(this.infoSelector));
      var fieldName = formInfo.field;
      if (this._inArray("all", collectList) || this._inArray(fieldName, collectList)) {
        formData[fieldName] = this._getFormFieldData(item);
      }
    }

    return formData;
  }

  HTMLSelector.prototype._getCookie = function(cookiename) {
    var name = cookiename + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
          var cv =  c.substring(name.length, c.length);
          return cv;
        }
    }
    return false;
  }

  HTMLSelector.prototype._getConsentLevelFromCookie = function () {
    return this._getCookie(this.consentCookie);
  }

  /**
   * Initialize the analyticstracker
   *
   *    the module returns a Singleton instance of the analyticstracker class
   *
   * @constructor analyticstracker();
   * @version 0.1.0
   * @author Stefan Maris
   * @copyright 2017 - MIT Licensed (http://www.opensource.org/licenses/mit-license.php)
   */
  var analyticstracker = function analyticstracker() {
    if (!(this instanceof analyticstracker)) {
      return new analyticstracker();
    }

    this.mediator = new Mediator();
    this._trackingSubscribers = [];
    this._internalTrackingQueue = [];
    this._internalTrackingHistory = [];
    this._htmlSelector = new HTMLSelector({});
    this._errorList = [];
    this._eventList = [];
    this._consent = this._htmlSelector._getConsentLevelFromCookie();
    this._version = "1.0.2";

    this.pageInfo = null;
  };

  analyticstracker.prototype.getInstance = function() {
    if (!_instance) {
        _instance = analyticstracker();
    }
    return _instance;
  }

  /**
   * Impression tracking handler
   *
   * @function
   * @param {string} selectorSettings selector Settings
   */
  analyticstracker.prototype.setHTMLSelectors = function (selectorSettings) {
    this._htmlSelector = new HTMLSelector(selectorSettings);
  }

  /* support for test environment */
  analyticstracker.prototype.resetTracker = function() {
    this._internalTrackingQueue = [];
    this._internalTrackingHistory = [];
    this._errorList = [];
    this._eventList = [];
    this._consent = this._htmlSelector._getConsentLevelFromCookie();
    this.pageInfo = null;
  }

  analyticstracker.prototype.getConsent = function() {
    this._consent = this._htmlSelector._getConsentLevelFromCookie();
    return this._consent;
  }

  analyticstracker.prototype.clone = function(a) {
    return JSON.parse(JSON.stringify(a));
  }

  analyticstracker.prototype.subscribe = function (subscribername, callback) {
    if (typeof callback === "function") {
      this._runHistory(subscribername, callback);
      this._trackingSubscribers.push({"name" : subscribername, "subscriber" : this.mediator.subscribe("tracking", callback)});
    }
    return false;
  };

  analyticstracker.prototype._runHistory = function (subscribername, callback) {
    /* playback tracking history if subscriber was added later on */
    if (this._internalTrackingHistory.length == 0) return;

    var predicate = function(eventname, data, id) {return id === subscriber.id};
    var subscriber = this.mediator.subscribe("tracking_history", callback, {"predicate": predicate});

    for (var i = 0, len = this._internalTrackingHistory.length; i < len; i++) {
      var item = this._internalTrackingHistory[i];
      this.mediator.publish("tracking_history", item.event, item, subscriber.id);
    }

    this.mediator.remove("tracking_history", subscriber.id);
  }

  analyticstracker.prototype._runQueue = function () {
    for (var i = 0, len = this._internalTrackingQueue.length; i < len; i++) {
      var item = this._internalTrackingQueue.shift();
      if (item.pageInfo == null) item.pageInfo = this.pageInfo;
      this.mediator.publish("tracking", item.event, item);
      this._internalTrackingHistory.push(item);
    }
  }

  analyticstracker.prototype._track = function (trackEventData) {
    var eventdata = this.clone(trackEventData);
    var preEmptiveQueue = false;

    if ((eventdata.event == this._htmlSelector.pageSelector)) {
      this.pageInfo = this.clone(eventdata.info);
      delete eventdata.info;
    }
    else {
      preEmptiveQueue = true;
    }

    if (this.pageInfo == null) {
      // cannot track without page info - queue the event for later consumption
      // Queue the event
      this._internalTrackingQueue.push(eventdata);
      return false;
    }

    eventdata.pageInfo = this.pageInfo;

    if (preEmptiveQueue) this._runQueue();

    this.mediator.publish("tracking", eventdata.event, eventdata);
    this._internalTrackingHistory.push(eventdata);

    if (! preEmptiveQueue) this._runQueue();
  }

  analyticstracker.prototype._isObject = function (obj) {
      return obj !== null && typeof obj === 'object';
  }

  analyticstracker.prototype._isPlainObject = function (obj) {
      return this._isObject(obj) && (
          obj.constructor === Object  // obj = {}
          || obj.constructor === undefined // obj = Object.create(null)
      );
  }

  analyticstracker.prototype._mergeDeep = function (target, sources) {
      if (! Array.isArray(sources)) {
        sources = [sources];
      }

      if (!sources.length) return target;
      var source = sources.shift();

      if(Array.isArray(target)) {
          if(Array.isArray(source)) {
              for (var i=0, len = source.length; i<len;i++) {
                target.push(source[i]);
              }
          } else {
              target.push(source);
          }
      } else if(this._isPlainObject(target)) {
          if(this._isPlainObject(source)) {
              for (var key in source) {
                if (source.hasOwnProperty(key)) {
                  if(!target[key]) {
                      target[key] = source[key];
                  } else {
                      this._mergeDeep(target[key], source[key]);
                  }
                }
              }
          } else {
              throw new Error("Cannot merge object with non-object");
          }
      } else {
          target = source;
      }
      return this._mergeDeep(target, sources);
  };

  /*
   * Public
   */

  analyticstracker.prototype.getErrors = function () {
    return this._errorList;
  }

  analyticstracker.prototype.getEventFromElement = function (theElements) {
    var theEvent = "";
    for (var i = 0, len = theElements.length; i < len; i++) {
      var item = theElements[i];
      if (item.hasAttribute(this._htmlSelector.eventSelector)) {
        theEvent = item.getAttribute(this._htmlSelector.eventSelector);
        return theEvent;
      }
    }
    return theEvent;
  }

  analyticstracker.prototype.trackElement = function (theElements, options) {
    try {
      var collector = [];

      if (typeof options === "undefined") {options = null;}
      /* check options */
      var addData  = ((options !== null) && options.hasOwnProperty("addData")) ? options.addData : null;
      var checkVisibility  = ((options !== null) && options.hasOwnProperty("checkVisibility")) ? options.checkVisibility : true;
      var doCollect  = ((options !== null) && options.hasOwnProperty("doCollect")) ? options.doCollect : false;
      var extendPageInfo = ((options !== null) && options.hasOwnProperty("extendPageInfo")) ? options.extendPageInfo : true;
      var changeEvent  = ((options !== null) && options.hasOwnProperty("changeEvent")) ? options.changeEvent : null;
      var newPage = ((options !== null) && options.hasOwnProperty("newPage")) ? options.newPage : false;
      var collectFormData = ((options !== null) && options.hasOwnProperty("collectFormData")) ? options.collectFormData : false;

      if (newPage) {
        /* indicates tracking of a new page - first clear all "DONE" trackImpressions */
        var elements = this._htmlSelector._selectElements('[' + this._htmlSelector.eventSelector + '$=-impression]');
        for (var i = 0, len = elements.length; i < len; i++) {
          var item = elements[i];
          if (this._htmlSelector._getItemType(item) == "impression") {
            item.removeAttribute(this._htmlSelector.doneSelector);
          }
        }
      }

      for (var i = 0, len = theElements.length; i < len; i++) {
        var item = theElements[i];
        if (checkVisibility && (this._htmlSelector._getItemType(item) == "impression") && (! this._htmlSelector.isVisible(item))) {
          continue;
        }

        var eventElement = {"event" : (changeEvent === null) ? item.getAttribute(this._htmlSelector.eventSelector) : changeEvent};

        if (eventElement.event.match(/impression/g) && (eventElement.event != this._htmlSelector.pageSelector) && item.getAttribute(this._htmlSelector.doneSelector)) {
          /* element already tracked */
          continue;
        }

        if (item.getAttribute(this._htmlSelector.infoSelector)) {eventElement.info = JSON.parse(item.getAttribute(this._htmlSelector.infoSelector));}
        if (item.getAttribute(this._htmlSelector.commerceSelector)) {eventElement.commerce = JSON.parse(item.getAttribute(this._htmlSelector.commerceSelector));}

        if ((eventElement.event == this._htmlSelector.pageSelector) && extendPageInfo && (this._htmlSelector.extendPageInfo.length > 0)) {
          for (var k = 0, klen = this._htmlSelector.extendPageInfo.length; k < klen; k++) {
            var infoField = this._htmlSelector.extendPageInfo[k];
            if (! eventElement.info.hasOwnProperty(infoField)) {
              eventElement.info = this._htmlSelector.setPageInfoField(eventElement.info, infoField);
            }
          }
        }

        if (addData !== null) {
          eventElement = this._mergeDeep(eventElement, addData);
        }

        if (collectFormData !== false) {
          var formData = {"info" : {"formdata" : this._htmlSelector._getFormData(item, collectFormData)}};
          eventElement = this._mergeDeep(eventElement, formData);
        }

        if (! doCollect) {
          this._track(eventElement);
        } else {
          collector.push(eventElement);
        }

        /* indicate that the element is tracked */
        if (this._htmlSelector._getItemType(item) == "impression") {
          item.setAttribute(this._htmlSelector.doneSelector, 'true');
        }
      }

      if (doCollect) {
        if (collector.length > 0) {
          var collectedEvent = {"event" : collector[0].event, "info" : [], "commerce" : []};

          for (var n = 0, len = collector.length; n < len; n++) {
            var item = collector.shift();
              if (item.info) {
                collectedEvent.info.push(item.info);
              }
              if (item.commerce) {
                collectedEvent.commerce.push(item.commerce);
              }
          }

          if (collectedEvent.info.length == 0) {delete collectedEvent.info;}
          if (collectedEvent.commerce.length == 0) {delete collectedEvent.commerce;}

          this._track(collectedEvent);
        }
      }
    } catch (e) {
      this._errorList.push(e);
      return false;
    }

    return true;
  }

  /**
   * Get current visible impressions
   * @function
   * @param {string} eventSelector impression data selector
   * @example analyticstracker.getVisibleImpressions("contentblock-impression");
   * return array of visible impressions items
   */
  analyticstracker.prototype.getVisibleImpressions = function (eventSelector) {
      try {
        var selector = '[' + this._htmlSelector.eventSelector + '="' + eventSelector + '"]:not([' + this._htmlSelector.doneSelector + '="true"])';
        var selectedElements = this._htmlSelector._selectElements(selector);
        var visibleElements = [];
        if (selectedElements.length) {
          for (var i = 0, len = selectedElements.length; i < len; i++) {
            var item = selectedElements[i];
            if ((this._htmlSelector._getItemType(item) == "impression") && (this._htmlSelector.isVisible(item))) {
              visibleElements.push(item);
            }
          }
        }
      } catch (e) {
        this._errorList.push(e);
        return false;
      }

      return visibleElements;
  }

  /**
   * Get current visible impressions
   * @function
   * @param {string} selector element selector, should return a single element
   * @example analyticstracker.isVisibleItem($("#testform"));
   * return array of visible impressions items
   */
  analyticstracker.prototype.isVisibleItem = function (selector) {
      try {
        var selectedElements = this._htmlSelector._selectElements(selector);
        var visibleElements = [];
        if (selectedElements.length) {
            var item = selectedElements[0];
            if (this._htmlSelector.isVisible(item)) {
              return true;
            }
        }
      } catch (e) {
        this._errorList.push(e);
        return false;
      }

      return false;
  }

  /**
   * Impression tracking handler
   * @function
   * @param {string} eventSelector impression data selector
   * @param {object} options optional tracking options (see {@link trackingOptions})
   * @example analyticstracker.trackImpression("page-impression");
   * @example analyticstracker.trackImpression("product-impression", {"doCollect" : true});
   */
  analyticstracker.prototype.trackImpression = function (eventSelector, options) {
      try {
        var selector = '[' + this._htmlSelector.eventSelector + '="' + eventSelector + '"]:not([' + this._htmlSelector.doneSelector + '="true"])';
        var selectedElements = this._htmlSelector._selectElements(selector);
        if (selectedElements.length) {
          this.trackElement(selectedElements, options);
        }
      } catch (e) {
        this._errorList.push(e);
        return false;
      }

      return true;
  }

  /**
   * Track event by passing event object
   * @function
   * @param {object} eventData event data object
   * @example analyticstracker.trackEvent({"event": "product-impression", "commerce" : {"id" : "12233", "name" : "someproduct", "price" : 1234.45}})
   */
  analyticstracker.prototype.trackEvent = function (eventData) {
    this._track(eventData);
    return true;
  }

  /**
   * Interaction tracking handlers
   *
   *    this function can directly be referenced in an event handler
   * @function
   * @param {object} event the browser event
   * @param {object} options optional tracking options (see {@link trackingOptions})
   * @example $('[data-tracking-event=product-click]').click(analyticstracker.trackInteraction)
   * @example $('[data-tracking-event=navigation-click]').click(function(e) {analyticstracker.trackInteraction(e, {"changeEvent" : "page-impression", "newPage" : true});});
   */
  analyticstracker.prototype.trackInteraction = function (event, options) {
  		return _instance.trackElement([event.currentTarget], options);
  }

  analyticstracker.prototype.trackIFrames = function (iframeURLs) {
    if (! Array.isArray(iframeURLs)) {
      throw new Error("Array expected");
      return false;
    }
    window.addEventListener("message", function (event) {
      // console.log(event);
      // check if event origin is in allowed iFrame list
      if (iframeURLs.indexOf(event.origin) > -1) {
        // Bug fix: only track GDDL events
        if (event.data.substring(0,9) == '{"event":') {
          var ev = JSON.parse(event.data);  
          _instance.trackEvent(ev);
        }
      }
    }, false);
  }

  analyticstracker.prototype.register = function (eventList) {
  		this._eventList = eventList;
  }

  analyticstracker.prototype.trackingSubscribe = analyticstracker.prototype.subscribe;

  return analyticstracker.prototype.getInstance;
}));
