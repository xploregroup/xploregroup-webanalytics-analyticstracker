var assert = require('assert');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

var doc = new JSDOM('<!DOCTYPE html><html><body id="content" data-tracking-event="page-impression" data-tracking-info=\'{"author":"stefan"}\'><div id="somepromotion" data-tracking-event="promotion-impression" data-tracking-info=\'{"campaign":"XYZ111","name":"BrandOutThere","detail":"DEF","placement":"top"}\'><a href="#" id="conversionbutton2" data-tracking-event="promotion-click" data-tracking-info=\'{"campaign":"XYZ111","name":"BrandOutThere","detail":"DEF","placement":"top"}\'><img src="images/promo2.jpg" width="100%"></a></div></body></html>', {
  url: "https://example.org/home.html?c=abcd&d=efg#part1",
  referrer: "https://example.com/",
  contentType: "text/html",
  userAgent: "Mellblomenator/9000",
  includeNodeLocations: true
});

global.window = doc.window;

var analyticstracker = require('../analyticstracker');

describe('analyticstracker', function () {

  var myinstance = analyticstracker();
  var testAssertiondata = {};
  var trackedEvent = false;
  var trackedEventData = [];

  function trackAssertion (someevent, someeventdata) {
    trackedEventData.push(someeventdata);
    trackedEvent = true;
  }

  function testEventData(trackedData, testData) {
    var someevent = testData.event;
    if (trackedData.pageInfo.timestamp) {trackedData.pageInfo.timestamp = ""};
    if (someevent == "page-impression") {
      assert.deepEqual(trackedData.pageInfo, testData.pageInfo);
    } else {
      assert.deepEqual(trackedData.pageInfo, testData.pageInfo);
      assert.deepEqual(trackedData.event, testData.event);
      assert.deepEqual(trackedData.info, testData.info);
      assert.deepEqual(trackedData.commerce, testData.commerce);
    }
  }

  it('should have mediator', function () {
    assert.equal(typeof myinstance.mediator, 'object');
  });

  it('should subscribe to the tracking channel', function () {
    myinstance.trackingSubscribe("test", trackAssertion);
    assert.equal(myinstance._trackingSubscribers.length, 1);
    assert.equal(myinstance._trackingSubscribers[0].name, "test");
    assert.equal(myinstance._trackingSubscribers[0].subscriber.channel.namespace, "tracking");
  });

if (1) {
  describe('Raw event mode', function() {
    it('track a page event', function(done) {
      trackedEvent = false;
      trackedEventData = [];
      testAssertiondata = {"event" : "page-impression", "pageInfo" : {"test" : "test"}};
      myinstance.trackEvent({"event" : "page-impression", "info" : {"test" : "test"}});
      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[0], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    it('track a banner event page-impression first', function(done) {
      myinstance.resetTracker();
      trackedEvent = false;
      trackedEventData = [];
      testAssertiondata = {"event" : "banner-impression", "pageInfo" : {"test" : "pagetest"}, "info" : {"test" : "test"}};
      myinstance.trackEvent({"event" : "page-impression", "info" : {"test" : "pagetest"}});
      myinstance.trackEvent({"event" : "banner-impression", "info" : {"test" : "test"}});
      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[1], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    it('track a product event page-impression first', function(done) {
      myinstance.resetTracker();
      trackedEvent = false;
      trackedEventData = [];
      testAssertiondata = {"event" : "product-impression", "pageInfo" : {"test" : "pagetest"}, "commerce" : {"test" : "product"}};
      myinstance.trackEvent({"event" : "page-impression", "info" : {"test" : "pagetest"}});
      myinstance.trackEvent({"event" : "product-impression", "commerce" : {"test" : "product"}});
      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[1], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    /* delayed page impression test, step 1 - page-impression not yet set */
    it('do not track a banner event without page-impression', function(done) {
      myinstance.resetTracker();
      trackedEvent = false;
      trackedEventData = [];
      myinstance.pageInfo = null;
      testAssertiondata = {"event" : "banner-impression", "pageInfo" : {"test" : "pagetest"}, "info" : {"test" : "test"}};
      myinstance.trackEvent({"event" : "banner-impression", "info" : {"test" : "test"}});
      function testImpressionEvent() {
        assert.equal(trackedEvent, false);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    /*
     * delayed page impression test, step 2 - fire page-impression
     * now the previous banner impression should be sent of...
     */
    it('track the banner event after the page-impression', function(done) {
      trackedEvent = false;
      testAssertiondata = {"event" : "banner-impression", "pageInfo" : {"test" : "pagetest"}, "info" : {"test" : "test"}};
      myinstance.trackEvent({"event" : "page-impression", "info" : {"test" : "pagetest"}});
      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[1], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });
  });
}
  describe('HTML mode', function() {
    myinstance.resetTracker();
    it('should pass page impression event', function(done) {
      trackedEvent = false;
      trackedEventData = [];
      testAssertiondata = {"event" : "page-impression",
                         "pageInfo" : { "author": 'stefan',
                                "url": 'https://example.org/home.html?c=abcd&d=efg#part1',
                                "domain": 'example.org',
                                "userAgent": 'Mellblomenator/9000',
                                "platform": '',
                                "referrer": 'https://example.com/',
                                "title": '',
                                "path": '/home.html',
                                "timestamp" : '',
                                "params": { "c": 'abcd', "d": 'efg' }
                              }};

      myinstance.trackImpression("page-impression", {"checkVisibility" : false});

      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[0], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    it('should pass a promotion impression event', function(done) {
      trackedEvent = false;
      testAssertiondata = {"event" : "promotion-impression",
                           "pageInfo" : { "author": 'stefan',
                                "url": 'https://example.org/home.html?c=abcd&d=efg#part1',
                                "userAgent": 'Mellblomenator/9000',
                                "domain": 'example.org',
                                "platform": '',
                                "referrer": 'https://example.com/',
                                "title": '',
                                "path": '/home.html',
                                "timestamp" : '',
                                "params": { "c": 'abcd', "d": 'efg' }
                              },
                            "info" : {"campaign":'XYZ111',
                                      "name":'BrandOutThere',
                                      "detail":'DEF',
                                      "placement":'top'}
                            };

      myinstance.trackImpression("promotion-impression", {"checkVisibility" : false});

      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[1], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });

    it('on click should pass a promotion click event', function(done) {
      trackedEvent = false;
      testAssertiondata = {"event" : "promotion-click",
                           "pageInfo" : { "author": 'stefan',
                                "url": 'https://example.org/home.html?c=abcd&d=efg#part1',
                                "userAgent": 'Mellblomenator/9000',
                                "domain": 'example.org',
                                "platform": '',
                                "referrer": 'https://example.com/',
                                "title": '',
                                "path": '/home.html',
                                "timestamp": '',
                                "params": { "c": 'abcd', "d": 'efg' }
                              },
                            "info" : {"campaign":'XYZ111',
                                      "name":'BrandOutThere',
                                      "detail":'DEF',
                                      "placement":'top'}
                            };

      window.document.getElementById("conversionbutton2").addEventListener("click", function(e) {
        myinstance.trackInteraction (e);
      }, false);
      window.document.getElementById("conversionbutton2").click();

      function testImpressionEvent() {
        assert.equal(trackedEvent, true);
        testEventData(trackedEventData[2], testAssertiondata);
        done();
      }
      setTimeout(testImpressionEvent, 100);
    });
  });
});
